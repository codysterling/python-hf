
                                 *****************
                                 * O   R   C   A *
                                 *****************

                                            #,                                       
                                            ###                                      
                                            ####                                     
                                            #####                                    
                                            ######                                   
                                           ########,                                 
                                     ,,################,,,,,                         
                               ,,#################################,,                 
                          ,,##########################################,,             
                       ,#########################################, ''#####,          
                    ,#############################################,,   '####,        
                  ,##################################################,,,,####,       
                ,###########''''           ''''###############################       
              ,#####''   ,,,,##########,,,,          '''####'''          '####       
            ,##' ,,,,###########################,,,                        '##       
           ' ,,###''''                  '''############,,,                           
         ,,##''                                '''############,,,,        ,,,,,,###''
      ,#''                                            '''#######################'''  
     '                                                          ''''####''''         
             ,#######,   #######,   ,#######,      ##                                
            ,#'     '#,  ##    ##  ,#'     '#,    #''#        ######   ,####,        
            ##       ##  ##   ,#'  ##            #'  '#       #        #'  '#        
            ##       ##  #######   ##           ,######,      #####,   #    #        
            '#,     ,#'  ##    ##  '#,     ,#' ,#      #,         ##   #,  ,#        
             '#######'   ##     ##  '#######'  #'      '#     #####' # '####'        



                  #######################################################
                  #                        -***-                        #
                  #          Department of theory and spectroscopy      #
                  #    Directorship and core code : Frank Neese         #
                  #        Max Planck Institute fuer Kohlenforschung    #
                  #                Kaiser Wilhelm Platz 1               #
                  #                 D-45470 Muelheim/Ruhr               #
                  #                      Germany                        #
                  #                                                     #
                  #                  All rights reserved                #
                  #                        -***-                        #
                  #######################################################


                         Program Version 5.0.4 -  RELEASE  -


 With contributions from (in alphabetic order):
   Daniel Aravena         : Magnetic Suceptibility
   Michael Atanasov       : Ab Initio Ligand Field Theory (pilot matlab implementation)
   Alexander A. Auer      : GIAO ZORA, VPT2 properties, NMR spectrum
   Ute Becker             : Parallelization
   Giovanni Bistoni       : ED, misc. LED, open-shell LED, HFLD
   Martin Brehm           : Molecular dynamics
   Dmytro Bykov           : SCF Hessian
   Vijay G. Chilkuri      : MRCI spin determinant printing, contributions to CSF-ICE
   Dipayan Datta          : RHF DLPNO-CCSD density
   Achintya Kumar Dutta   : EOM-CC, STEOM-CC
   Dmitry Ganyushin       : Spin-Orbit,Spin-Spin,Magnetic field MRCI
   Miquel Garcia          : C-PCM and meta-GGA Hessian, CC/C-PCM, Gaussian charge scheme
   Yang Guo               : DLPNO-NEVPT2, F12-NEVPT2, CIM, IAO-localization
   Andreas Hansen         : Spin unrestricted coupled pair/coupled cluster methods
   Benjamin Helmich-Paris : MC-RPA, TRAH-SCF, COSX integrals
   Lee Huntington         : MR-EOM, pCC
   Robert Izsak           : Overlap fitted RIJCOSX, COSX-SCS-MP3, EOM
   Marcus Kettner         : VPT2
   Christian Kollmar      : KDIIS, OOCD, Brueckner-CCSD(T), CCSD density, CASPT2, CASPT2-K
   Simone Kossmann        : Meta GGA functionals, TD-DFT gradient, OOMP2, MP2 Hessian
   Martin Krupicka        : Initial AUTO-CI
   Lucas Lang             : DCDCAS
   Marvin Lechner         : AUTO-CI (C++ implementation), FIC-MRCC
   Dagmar Lenk            : GEPOL surface, SMD
   Dimitrios Liakos       : Extrapolation schemes; Compound Job, initial MDCI parallelization
   Dimitrios Manganas     : Further ROCIS development; embedding schemes
   Dimitrios Pantazis     : SARC Basis sets
   Anastasios Papadopoulos: AUTO-CI, single reference methods and gradients
   Taras Petrenko         : DFT Hessian,TD-DFT gradient, ASA, ECA, R-Raman, ABS, FL, XAS/XES, NRVS
   Peter Pinski           : DLPNO-MP2, DLPNO-MP2 Gradient
   Christoph Reimann      : Effective Core Potentials
   Marius Retegan         : Local ZFS, SOC
   Christoph Riplinger    : Optimizer, TS searches, QM/MM, DLPNO-CCSD(T), (RO)-DLPNO pert. Triples
   Tobias Risthaus        : Range-separated hybrids, TD-DFT gradient, RPA, STAB
   Michael Roemelt        : Original ROCIS implementation
   Masaaki Saitow         : Open-shell DLPNO-CCSD energy and density
   Barbara Sandhoefer     : DKH picture change effects
   Avijit Sen             : IP-ROCIS
   Kantharuban Sivalingam : CASSCF convergence, NEVPT2, FIC-MRCI
   Bernardo de Souza      : ESD, SOC TD-DFT
   Georgi Stoychev        : AutoAux, RI-MP2 NMR, DLPNO-MP2 response
   Willem Van den Heuvel  : Paramagnetic NMR
   Boris Wezisla          : Elementary symmetry handling
   Frank Wennmohs         : Technical directorship


 We gratefully acknowledge several colleagues who have allowed us to
 interface, adapt or use parts of their codes:
   Stefan Grimme, W. Hujo, H. Kruse, P. Pracht,  : VdW corrections, initial TS optimization,
                  C. Bannwarth, S. Ehlert          DFT functionals, gCP, sTDA/sTD-DF
   Ed Valeev, F. Pavosevic, A. Kumar             : LibInt (2-el integral package), F12 methods
   Garnet Chan, S. Sharma, J. Yang, R. Olivares  : DMRG
   Ulf Ekstrom                                   : XCFun DFT Library
   Mihaly Kallay                                 : mrcc  (arbitrary order and MRCC methods)
   Jiri Pittner, Ondrej Demel                    : Mk-CCSD
   Frank Weinhold                                : gennbo (NPA and NBO analysis)
   Christopher J. Cramer and Donald G. Truhlar   : smd solvation model
   Lars Goerigk                                  : TD-DFT with DH, B97 family of functionals
   V. Asgeirsson, H. Jonsson                     : NEB implementation
   FAccTs GmbH                                   : IRC, NEB, NEB-TS, DLPNO-Multilevel, CI-OPT
                                                   MM, QMMM, 2- and 3-layer-ONIOM, Crystal-QMMM,
                                                   LR-CPCM, SF, NACMEs, symmetry and pop. for TD-DFT,
                                                   nearIR, NL-DFT gradient (VV10), updates on ESD,
                                                   ML-optimized integration grids
   S Lehtola, MJT Oliveira, MAL Marques          : LibXC Library
   Liviu Ungur et al                             : ANISO software


 Your calculation uses the libint2 library for the computation of 2-el integrals
 For citations please refer to: http://libint.valeyev.net

 Your ORCA version has been built with support for libXC version: 5.1.0
 For citations please refer to: https://tddft.org/programs/libxc/

 This ORCA versions uses:
   CBLAS   interface :  Fast vector & matrix operations
   LAPACKE interface :  Fast linear algebra routines
   SCALAPACK package :  Parallel linear algebra routines
   Shared memory     :  Shared parallel matrices
   BLAS/LAPACK       :  OpenBLAS 0.3.15  USE64BITINT DYNAMIC_ARCH NO_AFFINITY Zen SINGLE_THREADED
        Core in use  :  Zen
   Copyright (c) 2011-2014, The OpenBLAS Project




***************************************
The coordinates will be read from file: h2.xyz
***************************************


================================================================================

----- Orbital basis set information -----
Your calculation utilizes the basis: STO-3G
   H-Ne       : W. J. Hehre, R. F. Stewart and J. A. Pople, J. Chem. Phys. 2657 (1969).
   Na-Ar      : W. J. Hehre, R. Ditchfield, R. F. Stewart and J. A. Pople, J. Chem. Phys. 2769 (1970).
   K,Ca,Ga-Kr : W. J. Pietro, B. A. Levy, W. J. Hehre and R. F. Stewart, J. Am. Chem. Soc. 19, 2225 (1980).
   Sc-Zn,Y-Cd : W. J. Pietro and W. J. Hehre, J. Comp. Chem. 4, 241 (1983).

================================================================================
                                        WARNINGS
                       Please study these warnings very carefully!
================================================================================


WARNING: your system is open-shell and RHF/RKS was chosen
  ===> : WILL SWITCH to UHF/UKS


INFO   : the flag for use of the SHARK integral package has been found!

================================================================================
                                       INPUT FILE
================================================================================
NAME = h2p.inp
|  1> ! HF STO-3G LARGEPRINT
|  2> 
|  3> %output
|  4> 	Print[ P_Basis ] 2
|  5> 	Print[ P_MOs ] 1
|  6> end
|  7> 
|  8> * xyzfile 1 2 h2.xyz
|  9> 
| 10>                          ****END OF INPUT****
================================================================================

                       ****************************
                       * Single Point Calculation *
                       ****************************

---------------------------------
CARTESIAN COORDINATES (ANGSTROEM)
---------------------------------
  H      0.000000    0.000000    0.000000
  H      0.000000    0.000000    0.740852

----------------------------
CARTESIAN COORDINATES (A.U.)
----------------------------
  NO LB      ZA    FRAG     MASS         X           Y           Z
   0 H     1.0000    0     1.008    0.000000    0.000000    0.000000
   1 H     1.0000    0     1.008    0.000000    0.000000    1.400007

--------------------------------
INTERNAL COORDINATES (ANGSTROEM)
--------------------------------
 H      0   0   0     0.000000000000     0.00000000     0.00000000
 H      1   0   0     0.740852000000     0.00000000     0.00000000

---------------------------
INTERNAL COORDINATES (A.U.)
---------------------------
 H      0   0   0     0.000000000000     0.00000000     0.00000000
 H      1   0   0     1.400007385768     0.00000000     0.00000000

---------------------
BASIS SET INFORMATION
---------------------
There are 1 groups of distinct atoms

 Group   1 Type H   : 3s contracted to 1s pattern {3}

Atom   0H    basis set group =>   1
Atom   1H    basis set group =>   1

-------------------------
BASIS SET IN INPUT FORMAT
-------------------------

 # Basis set for element : H 
 NewGTO H 
 S 3 
   1       3.4252509100      0.1543289707
   2       0.6239137300      0.5353281424
   3       0.1688554000      0.4446345420
  end;

------------------------------------------------------------------------------
                           ORCA GTO INTEGRAL CALCULATION
------------------------------------------------------------------------------
------------------------------------------------------------------------------
                   ___                                                        
                  /   \      - P O W E R E D   B Y -                         
                 /     \                                                     
                 |  |  |   _    _      __       _____    __    __             
                 |  |  |  | |  | |    /  \     |  _  \  |  |  /  |          
                  \  \/   | |  | |   /    \    | | | |  |  | /  /          
                 / \  \   | |__| |  /  /\  \   | |_| |  |  |/  /          
                |  |  |   |  __  | /  /__\  \  |    /   |      \           
                |  |  |   | |  | | |   __   |  |    \   |  |\   \          
                \     /   | |  | | |  |  |  |  | |\  \  |  | \   \       
                 \___/    |_|  |_| |__|  |__|  |_| \__\ |__|  \__/        
                                                                              
                      - O R C A' S   B I G   F R I E N D -                    
                                      &                                       
                       - I N T E G R A L  F E E D E R -                       
                                                                              
 v1 FN, 2020, v2 2021                                                         
------------------------------------------------------------------------------


Reading SHARK input file h2p.SHARKINP.tmp ... SHARK General Contraction Test:  Segmented basis detected
   -> Pre-screening matrix *NOT* found on disk - recalculating with NShells=2
   -> Leaving CheckPreScreeningMatrix
ok
----------------------
SHARK INTEGRAL PACKAGE
----------------------

Number of atoms                             ...      2
Number of basis functions                   ...      2
Number of shells                            ...      2
Maximum angular momentum                    ...      0
Integral batch strategy                     ... SHARK/LIBINT Hybrid
RI-J (if used) integral strategy            ... SPLIT-RIJ (Revised 2003 algorithm where possible)
Printlevel                                  ...      2
Contraction scheme used                     ... SEGMENTED contraction
Coulomb Range Separation                    ... NOT USED
Exchange Range Separation                   ... NOT USED
Finite Nucleus Model                        ... NOT USED
Auxiliary Coulomb fitting basis             ... NOT available
Auxiliary J/K fitting basis                 ... NOT available
Auxiliary Correlation fitting basis         ... NOT available
Auxiliary 'external' fitting basis          ... NOT available
Integral threshold                          ...     1.000000e-10
Primitive cut-off                           ...     1.000000e-11
Primitive pair pre-selection threshold      ...     1.000000e-11

Calculating pre-screening integrals         ... done (  0.0 sec) Dimension = 2
Organizing shell pair data                  ... done (  0.0 sec)
Shell pair information
Total number of shell pairs                 ...         3
Shell pairs after pre-screening             ...         3
Total number of primitive shell pairs       ...        27
Primitive shell pairs kept                  ...        27
          la=0 lb=0:      3 shell pairs

Calculating one electron integrals          ... done (  0.0 sec)
Calculating Nuclear repulsion               ... done (  0.0 sec) ENN=      0.714281946057 Eh

SHARK setup successfully completed in   0.0 seconds

Maximum memory used throughout the entire GTOINT-calculation: 0.8 MB
-------------------------------------------------------------------------------
                                 ORCA SCF
-------------------------------------------------------------------------------

------------
SCF SETTINGS
------------
Hamiltonian:
 Ab initio Hamiltonian  Method          .... Hartree-Fock(GTOs)


General Settings:
 Integral files         IntName         .... h2p
 Hartree-Fock type      HFTyp           .... UHF
 Total Charge           Charge          ....    1
 Multiplicity           Mult            ....    2
 Number of Electrons    NEL             ....    1
 Basis Dimension        Dim             ....    2
 Nuclear Repulsion      ENuc            ....      0.7142819461 Eh

Convergence Acceleration:
 DIIS                   CNVDIIS         .... on
   Start iteration      DIISMaxIt       ....    12
   Startup error        DIISStart       ....  0.200000
   # of expansion vecs  DIISMaxEq       ....     5
   Bias factor          DIISBfac        ....   1.050
   Max. coefficient     DIISMaxC        ....  10.000
 Trust-Rad. Augm. Hess. CNVTRAH         .... auto
   Auto Start mean grad. ratio tolernc. ....  1.125000
   Auto Start start iteration           ....    20
   Auto Start num. interpolation iter.  ....    10
   Max. Number of Micro iterations      ....    16
   Max. Number of Macro iterations      .... Maxiter - #DIIS iter
   Number of Davidson start vectors     ....     2
   Converg. threshold I  (grad. norm)   ....   5.000e-05
   Converg. threshold II (energy diff.) ....   1.000e-06
   Grad. Scal. Fac. for Micro threshold ....   0.100
   Minimum threshold for Micro iter.    ....   0.010
   NR start threshold (gradient norm)   ....   0.001
   Initial trust radius                 ....   0.400
   Minimum AH scaling param. (alpha)    ....   1.000
   Maximum AH scaling param. (alpha)    .... 1000.000
   Orbital update algorithm             .... Taylor
   White noise on init. David. guess    .... on
   Maximum white noise                  ....   0.010
   Quad. conv. algorithm                .... NR
 SOSCF                  CNVSOSCF        .... off
 Level Shifting         CNVShift        .... on
   Level shift para.    LevelShift      ....    0.2500
   Turn off err/grad.   ShiftErr        ....    0.0010
 Zerner damping         CNVZerner       .... off
 Static damping         CNVDamp         .... on
   Fraction old density DampFac         ....    0.7000
   Max. Damping (<1)    DampMax         ....    0.9800
   Min. Damping (>=0)   DampMin         ....    0.0000
   Turn off err/grad.   DampErr         ....    0.1000
 Fernandez-Rico         CNVRico         .... off

SCF Procedure:
 Maximum # iterations   MaxIter         ....   125
 SCF integral mode      SCFMode         .... Direct
   Integral package                     .... SHARK and LIBINT hybrid scheme
 Reset frequency        DirectResetFreq ....    20
 Integral Threshold     Thresh          ....  1.000e-10 Eh
 Primitive CutOff       TCut            ....  1.000e-11 Eh

Convergence Tolerance:
 Convergence Check Mode ConvCheckMode   .... Total+1el-Energy
 Convergence forced     ConvForced      .... 0
 Energy Change          TolE            ....  1.000e-06 Eh
 1-El. energy change                    ....  1.000e-03 Eh
 DIIS Error             TolErr          ....  1.000e-06


Diagonalization of the overlap matrix:
Smallest eigenvalue                        ... 3.407e-01
Time for diagonalization                   ...    0.003 sec
Threshold for overlap eigenvalues          ... 1.000e-08
Number of eigenvalues below threshold      ... 0
Time for construction of square roots      ...    0.002 sec
Total time needed                          ...    0.005 sec

SHARK General Contraction Test:  Segmented basis detected
   -> Pre-screening matrix found on disk - Trying to read with NShells=2
   -> Passing Pre-screening matrix on to SHARK ...ok
   -> Leaving CheckPreScreeningMatrix
----------------------
SHARK INTEGRAL PACKAGE
----------------------

Number of atoms                             ...      2
Number of basis functions                   ...      2
Number of shells                            ...      2
Maximum angular momentum                    ...      0
Integral batch strategy                     ... SHARK/LIBINT Hybrid
RI-J (if used) integral strategy            ... SPLIT-RIJ (Revised 2003 algorithm where possible)
Printlevel                                  ...      2
Contraction scheme used                     ... SEGMENTED contraction
Coulomb Range Separation                    ... NOT USED
Exchange Range Separation                   ... NOT USED
Finite Nucleus Model                        ... NOT USED
Auxiliary Coulomb fitting basis             ... NOT available
Auxiliary J/K fitting basis                 ... NOT available
Auxiliary Correlation fitting basis         ... NOT available
Auxiliary 'external' fitting basis          ... NOT available
Integral threshold                          ...     1.000000e-10
Primitive cut-off                           ...     1.000000e-11
Primitive pair pre-selection threshold      ...     1.000000e-11

Time for model grid setup =    0.008 sec

------------------------------
INITIAL GUESS: MODEL POTENTIAL
------------------------------
Loading Hartree-Fock densities                     ... done
Calculating cut-offs                               ... done
Initializing the effective Hamiltonian             ... done
Setting up the integral package (SHARK)            ... done
Starting the Coulomb interaction                   ...     -> Generating integrals - 
    -> L=0 0 :     0.001 sec done=        30 skipped=         0 prim-skipped=         0
done (   0.0 sec)
Reading the grid                                   ... done
Mapping shells                                     ... done
Starting the XC term evaluation                    ... done (   0.0 sec)
Transforming the Hamiltonian                       ... done (   0.0 sec)
Diagonalizing the Hamiltonian                      ... done (   0.0 sec)
Back transforming the eigenvectors                 ... done (   0.0 sec)
Now organizing SCF variables                       ... done
                      ------------------
                      INITIAL GUESS DONE (   0.0 sec)
                      ------------------
----------------------
INITIAL GUESS ORBITALS
----------------------
                      0         1   
                  -0.44836   0.30563
                   1.00000   0.00000
                  --------  --------
  0H   1s         0.548934 -1.211460
  1H   1s         0.548934  1.211460
--------------
SCF ITERATIONS
--------------
               ***  Starting incremental Fock matrix formation  ***
===> SHARK/Fock/General/Sym: HFTyp=2 DoJ=1 DoX=1 facj= 2.000 facx=-1.000 NFock=1 NumOp=2 NMat=2 RangeSep=0 GeneralContraction=0 PGCOpt=-1
    -> L=0 0 0 0:     0.001 sec done=         6 (=100.0%) skipped=         0 
    -> UHF LowL loop time =     0.001 sec
    ->Total SHARK integral loop time =    0.000 sec
    ->Total LIBINT loop time =    0.001 sec

                         ----------------------------
                         !        ITERATION     0   !
                         ----------------------------
   Total Energy        :      -0.538512753666 Eh
   Energy Change       :      -0.538512753666 Eh
   MAX-DP              :       0.000000000000
   RMS-DP              :       0.000000000000
   Actual Damping      :       0.7000
   Actual Level Shift  :       0.2500 Eh

===> SHARK/Fock/General/Sym: HFTyp=2 DoJ=1 DoX=1 facj= 2.000 facx=-1.000 NFock=1 NumOp=2 NMat=2 RangeSep=0 GeneralContraction=0 PGCOpt=-1
    -> L=0 0 0 0:     0.000 sec done=         0 (=  0.0%) skipped=         6 
    -> UHF LowL loop time =     0.000 sec
    ->Total SHARK integral loop time =    0.000 sec
    ->Total LIBINT loop time =    0.000 sec
                 **** Energy Check signals convergence ****

               *****************************************************
               *                     SUCCESS                       *
               *           SCF CONVERGED AFTER   1 CYCLES          *
               *****************************************************


----------------
TOTAL SCF ENERGY
----------------

Total Energy       :           -0.53851275 Eh             -14.65368 eV

Components:
Nuclear Repulsion  :            0.71428195 Eh              19.43660 eV
Electronic Energy  :           -1.25279470 Eh             -34.09028 eV
One Electron Energy:           -1.25279470 Eh             -34.09028 eV
Two Electron Energy:            0.00000000 Eh               0.00000 eV

Virial components:
Potential Energy   :           -1.13905203 Eh             -30.99518 eV
Kinetic Energy     :            0.60053927 Eh              16.34150 eV
Virial Ratio       :            1.89671530


---------------
SCF CONVERGENCE
---------------

  Last Energy change         ...    0.0000e+00  Tolerance :   1.0000e-06
  Last MAX-Density change    ...    1.1102e-16  Tolerance :   1.0000e-05
  Last RMS-Density change    ...    4.5325e-17  Tolerance :   1.0000e-06
  Last DIIS Error            ...    2.2204e-16  Tolerance :   1.0000e-06

             **** THE GBW FILE WAS UPDATED (h2p.gbw) ****
             **** DENSITY h2p.scfp WAS UPDATED ****
             **** ENERGY FILE WAS UPDATED (h2p.en.tmp) ****
----------------------
UHF SPIN CONTAMINATION
----------------------

Expectation value of <S**2>     :     0.750000
Ideal value S*(S+1) for S=0.5   :     0.750000
Deviation                       :     0.000000

             **** THE GBW FILE WAS UPDATED (h2p.gbw) ****
             **** DENSITY h2p.scfp WAS UPDATED ****
----------------
ORBITAL ENERGIES
----------------
                 SPIN UP ORBITALS
  NO   OCC          E(Eh)            E(eV) 
   0   1.0000      -1.252795       -34.0903 
   1   0.0000       0.006700         0.1823 

                 SPIN DOWN ORBITALS
  NO   OCC          E(Eh)            E(eV) 
   0   0.0000      -0.578201       -15.7337 
   1   0.0000       0.187959         5.1146 
------------------
MOLECULAR ORBITALS
------------------
                      0         1   
                  -1.25279   0.00670
                   1.00000   0.00000
                  --------  --------
  0H   1s         0.548934 -1.211460
  1H   1s         0.548934  1.211460

                      0         1   
                  -0.57820   0.18796
                   0.00000   0.00000
                  --------  --------
  0H   1s         0.548934 -1.211460
  1H   1s         0.548934  1.211460


-------
DENSITY
-------
                  0          1    
      0       0.301329   0.301329
      1       0.301329   0.301329

                  0          1    
      0       0.000000   0.000000
      1       0.000000   0.000000


------------
SPIN DENSITY
------------
                  0          1    
      0       0.301329   0.301329
      1       0.301329   0.301329



                    ********************************
                    * MULLIKEN POPULATION ANALYSIS *
                    ********************************

--------------------------------------------
MULLIKEN ATOMIC CHARGES AND SPIN POPULATIONS
--------------------------------------------
   0 H :    0.500000    0.500000
   1 H :    0.500000    0.500000
Sum of atomic charges         :    1.0000000
Sum of atomic spin populations:    1.0000000

---------------------------------------------
MULLIKEN ORBITAL CHARGES AND SPIN POPULAITONS
---------------------------------------------
The uncorrected charge+spin densities=density diagonals are given in parenthesis)
   0:   0H   1s           0.500000    0.500000 (  0.301329    0.301329)
   1:   1H   1s           0.500000    0.500000 (  0.301329    0.301329)
Sum of orbital charges         :    1.0000000
Sum of orbital spin populations:    1.0000000

-----------------------------------------------------
MULLIKEN REDUCED ORBITAL CHARGES AND SPIN POPULATIONS
-----------------------------------------------------
CHARGE
  0 H s       :     0.500000  s :     0.500000
  1 H s       :     0.500000  s :     0.500000

SPIN
  0 H s       :     0.500000  s :     0.500000
  1 H s       :     0.500000  s :     0.500000

------------------------
MULLIKEN OVERLAP CHARGES
------------------------
B(  0-H ,  1-H ) :   0.3973 

-----------------------------------
MULLIKEN ORBITAL POPULATIONS PER MO
-----------------------------------
THRESHOLD FOR PRINTING IS 0.1%
SPIN UP
                      0         1   
                  -1.25279   0.00670
                   1.00000   0.00000
                  --------  --------
  0H   1s             50.0      50.0
  1H   1s             50.0      50.0

SPIN DOWN
                      0         1   
                  -0.57820   0.18796
                   0.00000   0.00000
                  --------  --------
  0H   1s             50.0      50.0
  1H   1s             50.0      50.0


                     *******************************
                     * LOEWDIN POPULATION ANALYSIS *
                     *******************************

-------------------------------------------
LOEWDIN ATOMIC CHARGES AND SPIN POPULATIONS
-------------------------------------------
   0 H :    0.500000    0.500000
   1 H :    0.500000    0.500000

--------------------------------------------
LOEWDIN ORBITAL CHARGES AND SPIN POPULATIONS
--------------------------------------------
   0:   0H   1s           0.500000    0.500000
   1:   1H   1s           0.500000    0.500000

----------------------------------------------------
LOEWDIN REDUCED ORBITAL CHARGES AND SPIN POPULATIONS
----------------------------------------------------
CHARGE
  0 H s       :     0.500000  s :     0.500000
  1 H s       :     0.500000  s :     0.500000

SPIN
  0 H s       :     0.500000  s :     0.500000
  1 H s       :     0.500000  s :     0.500000

---------------------------------
LOEWDIN BOND ORDERS (THRESH 0.050000)
---------------------------------
B(  0-H ,  1-H ) :   0.2500 

----------------------------------
LOEWDIN ORBITAL POPULATIONS PER MO
----------------------------------
THRESHOLD FOR PRINTING IS 0.1%
SPIN UP
                      0         1   
                  -1.25279   0.00670
                   1.00000   0.00000
                  --------  --------
  0H   1s             50.0      50.0
  1H   1s             50.0      50.0

SPIN DOWN
                      0         1   
                  -0.57820   0.18796
                   0.00000   0.00000
                  --------  --------
  0H   1s             50.0      50.0
  1H   1s             50.0      50.0

------------------------------------------
LOEWDIN REDUCED ORBITAL POPULATIONS PER MO
-------------------------------------------
THRESHOLD FOR PRINTING IS 0.1%
SPIN UP
                      0         1   
                  -1.25279   0.00670
                   1.00000   0.00000
                  --------  --------
 0 H  s              50.0      50.0
 1 H  s              50.0      50.0

SPIN DOWN
                      0         1   
                  -0.57820   0.18796
                   0.00000   0.00000
                  --------  --------
 0 H  s              50.0      50.0
 1 H  s              50.0      50.0


                      *****************************
                      * MAYER POPULATION ANALYSIS *
                      *****************************

  NA   - Mulliken gross atomic population
  ZA   - Total nuclear charge
  QA   - Mulliken gross atomic charge
  VA   - Mayer's total valence
  BVA  - Mayer's bonded valence
  FA   - Mayer's free valence

  ATOM       NA         ZA         QA         VA         BVA        FA
  0 H      0.5000     1.0000     0.5000     0.7500     0.5000     0.2500
  1 H      0.5000     1.0000     0.5000     0.7500     0.5000     0.2500

  Mayer bond orders larger than 0.100000
B(  0-H ,  1-H ) :   0.5000 

Local spin analysis not attempted because no beta electrons available
-------
TIMINGS
-------

Total SCF time: 0 days 0 hours 0 min 0 sec 

Total time                  ....       0.113 sec
Sum of individual times     ....       0.018 sec  ( 16.1%)

Fock matrix formation       ....       0.002 sec  (  2.2%)
Diagonalization             ....       0.000 sec  (  0.1%)
Density matrix formation    ....       0.000 sec  (  0.0%)
Population analysis         ....       0.001 sec  (  1.2%)
Initial guess               ....       0.006 sec  (  5.4%)
Orbital Transformation      ....       0.000 sec  (  0.0%)
Orbital Orthonormalization  ....       0.000 sec  (  0.0%)
DIIS solution               ....       0.000 sec  (  0.0%)

Maximum memory used throughout the entire SCF-calculation: 223.1 MB

-------------------------   --------------------
FINAL SINGLE POINT ENERGY        -0.538512753666
-------------------------   --------------------


                            ***************************************
                            *     ORCA property calculations      *
                            ***************************************

                                    ---------------------
                                    Active property flags
                                    ---------------------
   (+) Dipole Moment


------------------------------------------------------------------------------
                       ORCA ELECTRIC PROPERTIES CALCULATION
------------------------------------------------------------------------------

Dipole Moment Calculation                       ... on
Quadrupole Moment Calculation                   ... off
Polarizability Calculation                      ... off
GBWName                                         ... h2p.gbw
Electron density                                ... h2p.scfp
SHARK General Contraction Test:  Segmented basis detected
   -> Pre-screening matrix found on disk - Trying to read with NShells=2
   -> Passing Pre-screening matrix on to SHARK ...ok
   -> Leaving CheckPreScreeningMatrix
----------------------
SHARK INTEGRAL PACKAGE
----------------------

Number of atoms                             ...      2
Number of basis functions                   ...      2
Number of shells                            ...      2
Maximum angular momentum                    ...      0
Integral batch strategy                     ... SHARK/LIBINT Hybrid
RI-J (if used) integral strategy            ... SPLIT-RIJ (Revised 2003 algorithm where possible)
Printlevel                                  ...      2
Contraction scheme used                     ... SEGMENTED contraction
Coulomb Range Separation                    ... NOT USED
Exchange Range Separation                   ... NOT USED
Finite Nucleus Model                        ... NOT USED
Auxiliary Coulomb fitting basis             ... NOT available
Auxiliary J/K fitting basis                 ... NOT available
Auxiliary Correlation fitting basis         ... NOT available
Auxiliary 'external' fitting basis          ... NOT available
Integral threshold                          ...     1.000000e-10
Primitive cut-off                           ...     1.000000e-11
Primitive pair pre-selection threshold      ...     1.000000e-11

The origin for moment calculation is the CENTER OF MASS  = ( 0.000000,  0.000000  0.700004)

-------------
DIPOLE MOMENT
-------------
                                X             Y             Z
Electronic contribution:     -0.00000      -0.00000       0.00000
Nuclear contribution   :      0.00000       0.00000       0.00000
                        -----------------------------------------
Total Dipole Moment    :      0.00000       0.00000       0.00000
                        -----------------------------------------
Magnitude (a.u.)       :      0.00000
Magnitude (Debye)      :      0.00000



--------------------
Rotational spectrum 
--------------------
 
Rotational constants in cm-1:     0.000000    60.940140    60.940140 
Rotational constants in MHz :     0.000000 1826939.443264 1826939.443264 

 Dipole components along the rotational axes: 
x,y,z [a.u.] :     0.000000     0.000000     0.000000 
x,y,z [Debye]:     0.000000     0.000000     0.000000 

 

Timings for individual modules:

Sum of individual times         ...        0.190 sec (=   0.003 min)
GTO integral calculation        ...        0.041 sec (=   0.001 min)  21.5 %
SCF iterations                  ...        0.149 sec (=   0.002 min)  78.5 %
                             ****ORCA TERMINATED NORMALLY****
TOTAL RUN TIME: 0 days 0 hours 0 minutes 0 seconds 291 msec
