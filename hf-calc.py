#!/usr/bin/env python3

"""
This code calculates the HF energy of small molecular systems using the Hartree-Fock formalism and STO-NG basis sets.
The logic of the code comes largely from the book "Modern Quantum Chemistry: Introduction to Advanced Electronic
Structure Theory" (revised edition) by Attila Szabo and Neil S. Ostlund.  Specifically, it follows the 12-step SCF
process outlined on page 146, denoted below with double comments "##", and the integrals are given from their analytic
forms derived in Appendix A beginning on page 410.
"""

import argparse
from functions import (calculate_energy, calculate_p_diff, generate_g_matrix, generate_p_matrix,
                       generate_starting_matrices, nuclear_repulsion, read_xyz)
from constants import charge_dict, get_basis_set, zeta_dict
import numpy as np


# Set up argparse
p = argparse.ArgumentParser(description="A simple function to calculate HF energy of small molecules",
                            formatter_class=argparse.ArgumentDefaultsHelpFormatter)
p.add_argument('-xyz', nargs=1, help="The XYZ file containing atomic positions in angstroms (Å)", type=str,
               metavar="FILENAME.xyz", default=["/home/cody/playground/hf-python/xyz-files/heh.xyz"])
p.add_argument('-b', '--basis', nargs=1, choices=["sto1g", "sto2g", "sto3g"], default="sto3g",
               help="The basis set for the HF calculation")
p.add_argument('-c', '--charge', nargs=1, type=int, default=[0], help="Charge of the system")
args = p.parse_args()

#####

## 1. Specify a molecule (set of nuclear coordinates, atomic numbers, and number of electrons) and basis set
# Read xyz file to get number of atoms, list of atom types, and list of atomic coordinates
n_atoms, xyz_data = read_xyz(xyz_file=args.xyz[0])

# Get number of electrons of the system
n_elec = sum((charge_dict[atom_type] for atom_type, _ in xyz_data)) - args.charge[0]
# Sanity check: if we have <1 electrons, quit here
if n_elec < 1:
  print(f"Invalid nr. of electrons: {n_elec}")
  print(f"Must have at least 1 electron for the calculations to work properly!")
  exit()

# Get list of d (coefficient) and a (exponent alpha) for the given basis set
d_list, a_list = get_basis_set(basis_set_name=args.basis)

## 2. Calculate all required molecular integrals (S, H_core, and M(uv|ls))
# Generating starting matrices for overlap (S), kinetic (T), potential (V) and two-electron (M) integrals
S, T, V, M = generate_starting_matrices(xyz_data=xyz_data, zeta_dict=zeta_dict, charge_dict=charge_dict,
                                        d_list=d_list, a_list=a_list)

# Making core Hamiltonian H_core = T + V from p. 172 (3.255)
H_core = T + V

## 3. Diagonalize the overlap matrix S and obtain a transformation matrix X
# Symmetric orthogonalization from p. 143 (3.167), explained on p. 173 (3.259-3.261)
# s, U = np.linalg.eig(S)
# s_minushalf = np.diag(s**(-1/2))
# X = np.dot(np.dot(U, s_minushalf), U.T)

# Canonical orthogonalization from p. 144 (3.169), explained on p. 174 (3.262)
s, U = np.linalg.eig(S)
s_minushalf = np.diag(s**(-1/2))
X = np.dot(U, s_minushalf)

## 4. Obtain a guess at the density matrix P (zeroes, from p. 148)
P = np.zeros(S.shape)

# Now do SCF loop while unconverged
E_list = []
n_iterations = 0
threshold = 100

while threshold > 10**-6:  # Converge threshold can be anything, usually 10**-6
  ## 5. Calculate matrix G from P and M(uv|ls)
  G = generate_g_matrix(P, M)

  ## 6. Add G to H_core to obtain F = H_core + G
  F = H_core + G

  ## 7. Calculate transformed Fock matrix F' = XtFX
  F_prime = np.dot(X.T, np.dot(F, X))

  ## 8. Diagonalize F' to obtain C' and epsilons
  epsilons, C_prime = np.linalg.eig(F_prime)

  ## 9. Calculate C = XC'
  C = np.dot(X, C_prime)

  electronic_energy = calculate_energy(P=P, H_core=H_core, F=F)
  E_list.append(electronic_energy)

  ## 10. Form a new density matrix P from C
  P_new = generate_p_matrix(C=C, n_elec=n_elec)

  ## 11. Determine convergence between P and P_new
  threshold = calculate_p_diff(P=P, P_new=P_new)

  # Set P_new to P for next iteration
  P = P_new
  n_iterations += 1

## 12. Converged: calculate energies
electronic_energy = calculate_energy(P=P, H_core=H_core, F=F) if n_elec > 1 else np.min(epsilons)
nuclear_repulsion_energy = nuclear_repulsion(xyz_data=xyz_data, charge_dict=charge_dict)
total_energy = electronic_energy + nuclear_repulsion_energy

print(f"Converged in {n_iterations} iterations")
print(f"Orbital energies (Eh) are {epsilons}")
print(f"Orbital matrix is:\n {C}")
print(f"Density bond order matrix is:\n {P}")
print(f"Elec_energy list (Eh) is {E_list}")
print(f"Electronic energy is {electronic_energy} Eh")
print(f"Nuclear repulsion is {nuclear_repulsion_energy} Eh")
print(f"Total energy is {total_energy} Eh")
