#!/usr/bin/env bash

source .venv/bin/activate
echo -n " H (-0.4665): "; ./hf-calc.py -xyz xyz-files/h.xyz | grep 'Total energy' | cut -d' ' -f4 | cut -b 1-7
echo -n " H_2 (-1.117): "; ./hf-calc.py -xyz xyz-files/h2.xyz | grep 'Total energy' | cut -d' ' -f4 | cut -b 1-7
echo -n "[H_2]+ (-0.539): "; ./hf-calc.py -xyz xyz-files/h2.xyz -c 1 2>/dev/null | grep 'Total energy' | cut -d' ' -f4 | cut -b 1-7
echo -n " He (-2.6438): "; ./hf-calc.py -xyz xyz-files/he.xyz | grep 'Total energy' | cut -d' ' -f4 | cut -b 1-7
echo -n "[HeH]+ (-2.8606): "; ./hf-calc.py -xyz xyz-files/heh.xyz -c 1 | grep 'Total energy' | cut -d' ' -f4 | cut -b 1-7
