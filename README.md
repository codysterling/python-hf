# STO-NG Hartree-Fock in Python

This code calculates the HF energy of small molecular systems using the Hartree-Fock formalism and STO-NG basis sets.  The logic of the code comes largely from the book "Modern Quantum Chemistry: Introduction to Advanced Electronic Structure Theory" (revised edition) by Attila Szabo and Neil S. Ostlund.
