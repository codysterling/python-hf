import numpy as np
import math


def read_xyz(xyz_file: str) -> tuple[int, list]:
  xyz_data = []

  with open(xyz_file) as f:
    n_atoms = int(f.readline().strip())
    f.readline()  # ignore comment line in file
    for line in f.read().splitlines():
      atom_type, *(atom_pos) = line.split()
      xyz_data.append((atom_type, np.array([float(i) for i in atom_pos])))

  if n_atoms != len(xyz_data):
    print(f"n_atoms: {n_atoms} != number of entries: {len(xyz_data)}!")
    exit()

  return n_atoms, xyz_data


# Normalization prefactor for a Gaussian-type orbital from p. 153 (3.203)
def _K(alpha: float) -> float:
  return (2*alpha/math.pi)**(3/4)


# Overlap integral from Szabo and Ostlund p. 412 (A.9) with normalization factors
def _overlap_int(A: tuple[float, np.array], B: tuple[float, np.array]) -> float:
  a, Ra = A
  b, Rb = B
  p = a + b
  return (math.pi/p)**(3/2) * math.exp(-a*b/p * np.linalg.norm(Ra-Rb)**2) * _K(a) * _K(b)


# Kinetic energy integral from Szabo and Ostlund p. 412 (A.11) with normalization factors
def _kinetic_int(A: tuple[float, np.array], B: tuple[float, np.array]) -> float:
  a, Ra = A
  b, Rb = B
  p = a + b
  return (a*b/p * (3 - 2*a*b/p * np.linalg.norm(Ra-Rb)**2) * (math.pi/p)**(3/2) 
          * math.exp(-a*b/p * np.linalg.norm(Ra-Rb)**2) * _K(a) * _K(b))


# Fourier transform from Szabo and Ostlund p. 415 (A.32)
def _F0(t: float) -> float:
  return ((1/2) * (math.pi / t)**(1/2) * math.erf(t**(1/2))) if t else 1


# Nuclear attraction integral from Szabo and Ostlund p. 415 (A.33) with normalization factors
def _nuclear_attraction_int(A: tuple[float, np.array], B: tuple[float, np.array], Zc: int, Rc: np.array) -> float:
  a, Ra = A
  b, Rb = B
  p = a + b
  Rp = (a*Ra + b*Rb) / p
  left = -2*math.pi / p * Zc * math.exp(-a*b/p * np.linalg.norm(Ra-Rb)**2)
  right = _F0(p * np.linalg.norm(Rp-Rc)**2)
  return left * right * _K(a) * _K(b)

# Two-electron integrals from Szabo and Ostlund p. 416 (A.41) with normalization factors
def _two_electron_int(A: tuple[float, np.array], B: tuple[float, np.array], C: tuple[float, np.array],
                      D: tuple[float, np.array]) -> float:
  a, Ra = A
  b, Rb = B
  c, Rc = C
  d, Rd = D
  p, q = a + b, c + d
  Rp, Rq = (a*Ra + b*Rb) / p, (c*Rc + d*Rd) / q
  left = 2*math.pi**(5/2) / (p * q * (p + q)**(1/2))
  center = math.exp(-a*b/p * np.linalg.norm(Ra-Rb)**2 -c*d/q * np.linalg.norm(Rc-Rd)**2)
  right = _F0(p*q/(p+q) * np.linalg.norm(Rp-Rq)**2)
  return left * center * right * _K(a) * _K(b) * _K(c) * _K(d)


# Generating starting matrices for overlap (S), kinetic (T), potential (V1, V2) and two-electron (M) integrals
def generate_starting_matrices(xyz_data: list,
                               zeta_dict: dict[str, tuple],
                               charge_dict: dict[str, tuple],
                               d_list: np.array,
                               a_list: np.array,
                               ) -> tuple[np.array]:

  # Build up contracted Gaussian functions (CGFs) as a combination of primitive Gaussian function (GFs)
  # as introduced on p. 155 (3.212)
  orbitals = []
  for atom_type, atom_pos in xyz_data:
    for z_index, zeta in enumerate(zeta_dict[atom_type]):
      orbitals.append([(d, (a, atom_pos)) for d, a in zip(d_list[z_index], a_list[z_index]*(zeta**2))])
  basis_size = len(orbitals)

  # Initialize blank matrices to fill
  S = np.zeros([basis_size, basis_size])
  T = np.zeros([basis_size, basis_size])
  V = np.zeros([basis_size, basis_size])
  M = np.zeros([basis_size, basis_size, basis_size, basis_size])

  # Now iterate over the GFs in each CGF to build up the various matrices
  for idx_1, cgf_1 in enumerate(orbitals):
    for idx_2, cgf_2 in enumerate(orbitals):
      for d_1, gf_1 in cgf_1:
        for d_2, gf_2 in cgf_2:
          S[idx_1, idx_2] += d_1*d_2*_overlap_int(A=gf_1, B=gf_2)
          T[idx_1, idx_2] += d_1*d_2*_kinetic_int(A=gf_1, B=gf_2)

          for atom_type, atom_pos in xyz_data:
            V[idx_1, idx_2] += d_1*d_2*_nuclear_attraction_int(A=gf_1, B=gf_2, Zc=charge_dict[atom_type], Rc=atom_pos)

          for idx_3, cgf_3 in enumerate(orbitals):
            for idx_4, cgf_4 in enumerate(orbitals):
              for d_3, gf_3 in cgf_3:
                for d_4, gf_4 in cgf_4:
                  M[idx_1, idx_2, idx_3, idx_4] += d_1*d_2*d_3*d_4*_two_electron_int(A=gf_1, B=gf_2, C=gf_3, D=gf_4)

  # Return matrices
  return S, T, V, M


def generate_g_matrix(P: np.array, M: np.array) -> np.array:
  # Get basis size from P (square matrix)
  basis_size = P.shape[0]
  G = np.zeros(P.shape)
  for i in range(basis_size):
    for j in range(basis_size):
      for x in range(basis_size):
        for y in range(basis_size):
          G[i, j] += P[x, y] * (M[i, j, y, x] - (1/2)*M[i, x, y, j])
  return G


def generate_p_matrix(C: np.array, n_elec: int) -> np.array:
  P = np.zeros(C.shape)
  # Get basis size from P (square matrix)
  basis_size = P.shape[0]
  for i in range(basis_size):
    for j in range(basis_size):
      for e in range(n_elec//2):
        P[i, j] = 2 * C[i, e] * C[j, e]
  return P


def calculate_energy(P: np.array, H_core: np.array, F: np.array) -> float:
  # Get basis size from P (square matrix)
  basis_size = P.shape[0]
  energy = 0
  for u in range(basis_size):
    for v in range(basis_size):
      energy += (1/2) * P[v,u] * (H_core[u,v] + F[u,v])
  return energy


def calculate_p_diff(P: np.array, P_new: np.array) -> float:
  # Get basis size from P (square matrix)
  basis_size = P.shape[0]
  x = 0
  for i in range(basis_size):
    for j in range(basis_size):
      x += ((P[i, j] - P_new[i, j]) / basis_size)**2
  return x**(1/2)


def nuclear_repulsion(xyz_data: list, charge_dict: dict) -> float:
  nuc_repulsion = 0
  for index_a, (type_a, pos_a) in enumerate(xyz_data):
    for type_b, pos_b in xyz_data[index_a+1:]:
      nuc_repulsion += (charge_dict[type_a]*charge_dict[type_b])/np.linalg.norm(pos_a-pos_b)
  return nuc_repulsion
