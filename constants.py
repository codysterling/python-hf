import numpy as np


# From Table 3.8 on p. 186
zeta_dict = {
  'H': [1.24],
  'He': [2.0925],
  'Li': [2.69, 0.75],
  'Be': [3.68, 1.10],
}

charge_dict = {
  'H': 1,
  'He': 2,
  'Li': 3,
  'Be': 4,
}

# STO-3G from Table 3.7 on p. 185
def get_basis_set(basis_set_name: str):
  match basis_set_name:
    case "sto1g":
      d_list = np.array([
        [1.0],  # 1s
      ])
      a_list = np.array([
        [0.270950],  # 1s
      ])
    case "sto2g":
      d_list = np.array([
        [0.678914, 0.430129],  # 1s
      ])
      a_list = np.array([
        [0.151623, 0.851819],  # 1s
      ])
    case "sto3g":
      d_list = np.array([
        [0.444635, 0.535328, 0.154329],  # 1s
        [0.700115, 0.399513, -0.0999672],  # 2s
      ])
      a_list = np.array([
        [0.109818, 0.405771, 2.22766],  # 1s
        [0.0751386, 0.231031, 0.994203],  # 2sp
      ])
    case _:
      print(f"Basis set '{basis_set_name}' not found!")
      exit()
  return d_list, a_list
